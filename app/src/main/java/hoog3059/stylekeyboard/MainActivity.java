package hoog3059.stylekeyboard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
    public Switch switchAltIgpayAtinlay;
    private SharedPreferences keyboardPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        keyboardPreferences = getSharedPreferences("keyboardPreferences", MODE_PRIVATE);

        switchAltIgpayAtinlay = findViewById(R.id.switchAltIgpayAtinlay);
        switchAltIgpayAtinlay.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences keyboardPreferences = getSharedPreferences("keyboardPreferences", MODE_PRIVATE);
                SharedPreferences.Editor keyboardPreferencesEditor = keyboardPreferences.edit();
                keyboardPreferencesEditor.putBoolean("altIgpayAtinlay", isChecked);
                keyboardPreferencesEditor.apply();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        switchAltIgpayAtinlay.setChecked(keyboardPreferences.getBoolean("altIgpayAtinlay", false));
    }
}
