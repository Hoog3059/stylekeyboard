package hoog3059.stylekeyboard;

import android.content.SharedPreferences;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.*;

public class StyleKeyboardService extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    private HashMap<Character, String> scriptCharacters = new HashMap<Character, String>() {{
        put('A', "\ud835\udc9c");
        put('B', "\u212c");
        put('C', "\ud835\udc9e");
        put('D', "\ud835\udc9f");
        put('E', "\u2130");
        put('F', "\u2131");
        put('G', "\ud835\udca2");
        put('H', "\u210b");
        put('I', "\u2110");
        put('J', "\ud835\udca5");
        put('K', "\ud835\udca6");
        put('L', "\u2112");
        put('M', "\u2133");
        put('N', "\ud835\udca9");
        put('O', "\ud835\udcaa");
        put('P', "\ud835\udcab");
        put('Q', "\ud835\udcac");
        put('R', "\u211b");
        put('S', "\ud835\udcae");
        put('T', "\ud835\udcaf");
        put('U', "\ud835\udcb0");
        put('V', "\ud835\udcb1");
        put('W', "\ud835\udcb2");
        put('X', "\ud835\udcb3");
        put('Y', "\ud835\udcb4");
        put('Z', "\ud835\udcb5");
        put('a', "\ud835\udcb6");
        put('b', "\ud835\udcb7");
        put('c', "\ud835\udcb8");
        put('d', "\ud835\udcb9");
        put('e', "\u212f");
        put('f', "\ud835\udcbb");
        put('g', "\u210a");
        put('h', "\ud835\udcbd");
        put('i', "\ud835\udcbe");
        put('j', "\ud835\udcbf");
        put('k', "\ud835\udcc0");
        put('l', "\ud835\udcc1");
        put('m', "\ud835\udcc2");
        put('n', "\ud835\udcc3");
        put('o', "\u2134");
        put('p', "\ud835\udcc5");
        put('q', "\ud835\udcc6");
        put('r', "\ud835\udcc7");
        put('s', "\ud835\udcc8");
        put('t', "\ud835\udcc9");
        put('u', "\ud835\udcca");
        put('v', "\ud835\udccb");
        put('w', "\ud835\udccc");
        put('x', "\ud835\udccd");
        put('y', "\ud835\udcce");
        put('z', "\ud835\udccf");
    }};
    private HashMap<Character, String> frakturCharacters = new HashMap<Character, String>() {{
        put('A', "\ud835\udd04");
        put('B', "\ud835\udd05");
        put('C', "\u212d");
        put('D', "\ud835\udd07");
        put('E', "\ud835\udd08");
        put('F', "\ud835\udd09");
        put('G', "\ud835\udd0a");
        put('H', "\u210c");
        put('I', "\u2111");
        put('J', "\ud835\udd0d");
        put('K', "\ud835\udd0e");
        put('L', "\ud835\udd0f");
        put('M', "\ud835\udd10");
        put('N', "\ud835\udd11");
        put('O', "\ud835\udd12");
        put('P', "\ud835\udd13");
        put('Q', "\ud835\udd14");
        put('R', "\u211c");
        put('S', "\ud835\udd16");
        put('T', "\ud835\udd17");
        put('U', "\ud835\udd18");
        put('V', "\ud835\udd19");
        put('W', "\ud835\udd1a");
        put('X', "\ud835\udd1b");
        put('Y', "\ud835\udd1c");
        put('Z', "\u2128");
        put('a', "\ud835\udd1e");
        put('b', "\ud835\udd1f");
        put('c', "\ud835\udd20");
        put('d', "\ud835\udd21");
        put('e', "\ud835\udd22");
        put('f', "\ud835\udd23");
        put('g', "\ud835\udd24");
        put('h', "\ud835\udd25");
        put('i', "\ud835\udd26");
        put('j', "\ud835\udd27");
        put('k', "\ud835\udd28");
        put('l', "\ud835\udd29");
        put('m', "\ud835\udd2a");
        put('n', "\ud835\udd2b");
        put('o', "\ud835\udd2c");
        put('p', "\ud835\udd2d");
        put('q', "\ud835\udd2e");
        put('r', "\ud835\udd2f");
        put('s', "\ud835\udd30");
        put('t', "\ud835\udd31");
        put('u', "\ud835\udd32");
        put('v', "\ud835\udd33");
        put('w', "\ud835\udd34");
        put('x', "\ud835\udd35");
        put('y', "\ud835\udd36");
        put('z', "\ud835\udd37");
    }};
    private HashMap<Character, String> doublestruckCharacters = new HashMap<Character, String>() {{
        put('A', "\ud835\udd38");
        put('B', "\ud835\udd39");
        put('C', "\u2102");
        put('D', "\ud835\udd3b");
        put('E', "\ud835\udd3c");
        put('F', "\ud835\udd3d");
        put('G', "\ud835\udd3e");
        put('H', "\u210d");
        put('I', "\ud835\udd40");
        put('J', "\ud835\udd41");
        put('K', "\ud835\udd42");
        put('L', "\ud835\udd43");
        put('M', "\ud835\udd44");
        put('N', "\u2115");
        put('O', "\ud835\udd46");
        put('P', "\u2119");
        put('Q', "\u211a");
        put('R', "\u211d");
        put('S', "\ud835\udd4a");
        put('T', "\ud835\udd4b");
        put('U', "\ud835\udd4c");
        put('V', "\ud835\udd4d");
        put('W', "\ud835\udd4e");
        put('X', "\ud835\udd4f");
        put('Y', "\ud835\udd50");
        put('Z', "\u2124");
        put('a', "\ud835\udd52");
        put('b', "\ud835\udd53");
        put('c', "\ud835\udd54");
        put('d', "\ud835\udd55");
        put('e', "\ud835\udd56");
        put('f', "\ud835\udd57");
        put('g', "\ud835\udd58");
        put('h', "\ud835\udd59");
        put('i', "\ud835\udd5a");
        put('j', "\ud835\udd5b");
        put('k', "\ud835\udd5c");
        put('l', "\ud835\udd5d");
        put('m', "\ud835\udd5e");
        put('n', "\ud835\udd5f");
        put('o', "\ud835\udd60");
        put('p', "\ud835\udd61");
        put('q', "\ud835\udd62");
        put('r', "\ud835\udd63");
        put('s', "\ud835\udd64");
        put('t', "\ud835\udd65");
        put('u', "\ud835\udd66");
        put('v', "\ud835\udd67");
        put('w', "\ud835\udd68");
        put('x', "\ud835\udd69");
        put('y', "\ud835\udd6a");
        put('z', "\ud835\udd6b");
    }};
    private ArrayList<Character> consonants = new ArrayList<Character>() {{
        add('b');
        add('c');
        add('d');
        add('f');
        add('g');
        add('h');
        add('j');
        add('k');
        add('l');
        add('m');
        add('n');
        add('p');
        add('q');
        add('r');
        add('s');
        add('t');
        add('v');
        add('w');
        add('x');
        add('z');
    }};
    private ArrayList<Character> vowels = new ArrayList<Character>() {{
        add('a');
        add('e');
        add('i');
        add('o');
        add('u');
        add('y');
    }};
    private String previousText = "";

    @Override
    public View onCreateInputView() {
        KeyboardView keyboardView = (KeyboardView) getLayoutInflater().inflate(R.layout.keyboard_view, null);
        Keyboard keyboard = new Keyboard(this, R.xml.styling_pad);
        keyboardView.setKeyboard(keyboard);
        keyboardView.setOnKeyboardActionListener(this);
        return keyboardView;
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;

        CharSequence selectedText = ic.getSelectedText(0);

        /*CODES
        0: change keyboard
        2: script
        3: fraktur
        4: double-struck
        5: alt-caps
        6: Igpay Atinlay
        -5: backspace
        -10: undo
         */
        switch (primaryCode) {
            case 0:
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();
                break;
            case 2:
                recordTextForUndo(ic);
                ic.commitText(textReplaceDictionary(selectedText, scriptCharacters), 1);
                break;
            case 3:
                recordTextForUndo(ic);
                ic.commitText(textReplaceDictionary(selectedText, frakturCharacters), 1);
                break;
            case 4:
                recordTextForUndo(ic);
                ic.commitText(textReplaceDictionary(selectedText, doublestruckCharacters), 1);
                break;
            case 5:
                recordTextForUndo(ic);
                if (!TextUtils.isEmpty(selectedText)) {
                    String selectedTextString = ((String) selectedText).toLowerCase();
                    StringBuilder newStringBuilder = new StringBuilder();
                    for (int i = 0; i < selectedText.length(); i++) {
                        if (selectedTextString.charAt(i) == 'r' || selectedTextString.charAt(i) == 'l' || selectedTextString.charAt(i) == 'j') {
                            newStringBuilder.append(Character.toUpperCase(selectedTextString.charAt(i)));
                        } else if ((i + 1) % 2 == 0) {
                            newStringBuilder.append(Character.toUpperCase(selectedTextString.charAt(i)));
                        } else {
                            newStringBuilder.append(selectedTextString.charAt(i));
                        }
                    }
                    ic.commitText(newStringBuilder.toString(), 1);
                }
                break;
            case 6:
                //Igpay Atinlay
                recordTextForUndo(ic);
                SharedPreferences keyboardPreferences = getSharedPreferences("keyboardPreferences", MODE_PRIVATE);
                ic.commitText(pigLatinConverter(selectedText, keyboardPreferences.getBoolean("altIgpayAtinlay", false)), 1);
                break;
            case -5:
                if (TextUtils.isEmpty(selectedText)) {
                    ic.deleteSurroundingText(1, 0);
                } else {
                    ic.commitText("", 1);
                }
                break;
            case -10:
                if (!previousText.isEmpty()) {
                    ic.deleteSurroundingText(999, 999);
                    ic.commitText(previousText, 1);
                }
                break;
        }
    }

    private void recordTextForUndo(InputConnection ic) {
        /*ic.setSelection(0, 999999);
        String selectedText = (String) ic.getSelectedText(0);
        previousText = selectedText;*/
    }

    private String textReplaceDictionary(CharSequence selectedText, HashMap<Character, String> dictionary) {
        if (!TextUtils.isEmpty(selectedText)) {
            StringBuilder newStringBuilder = new StringBuilder();
            for (int i = 0; i < selectedText.length(); i++) {
                newStringBuilder.append(dictionary.getOrDefault(selectedText.charAt(i), String.valueOf(selectedText.charAt(i))));
            }
            return newStringBuilder.toString();
        } else {
            return "";
        }
    }

    private String pigLatinConverter(CharSequence selectedText, boolean alternativeIgpayAtinlay) {
        StringBuilder output = new StringBuilder();

        Pattern splitPattern = Pattern.compile(" ");
        String[] words = splitPattern.split(selectedText);
        ArrayList<String> newWords = new ArrayList<String>();

        for (String word : words) {
            if (word.equals("")) {
                continue;
            }

            //Starts with consonant
            if (consonants.contains(word.toLowerCase().charAt(0))) {
                //Check for consonant group
                int firstVowelIndex = 0;
                for (int i = 0; i < word.length(); i++) {
                    if (vowels.contains(word.toLowerCase().charAt(i))) {
                        firstVowelIndex = i;
                        break;
                    }
                }

                //Check if word starts with upper case
                boolean startWithUpperCase = Character.isUpperCase(word.charAt(0));

                //Construct new word
                String newWord = word.substring(firstVowelIndex) + word.substring(0, firstVowelIndex).toLowerCase() + "ay";
                if (startWithUpperCase) {
                    char firstUpperCaseCharacter = Character.toUpperCase(newWord.charAt(0));
                    newWord = firstUpperCaseCharacter + newWord.substring(1);
                }
                newWords.add(newWord);
            }

            //Starts with vowel
            else if (vowels.contains(word.charAt(0))) {
                //If true, move beginning vowel with following consonant (group)
                //to the back of the word:
                //Ex:
                //    True:  elegant --> egantelyay
                //    False: elegant --> elegantyay
                if (alternativeIgpayAtinlay) {
                    //Find first consonant group
                    int secondVowelGroup = 0;
                    boolean firstConsonantFound = false;
                    for (int i = 0; i < word.length(); i++) {
                        if (vowels.contains(word.toLowerCase().charAt(i)) && firstConsonantFound) {
                            secondVowelGroup = i;
                            break;
                        }

                        if (consonants.contains(word.toLowerCase().charAt(i))) {
                            firstConsonantFound = true;
                        }
                    }

                    //Check if word starts with upper case
                    boolean startWithUpperCase = Character.isUpperCase(word.charAt(0));

                    //Construct new word
                    String newWord = word.substring(secondVowelGroup) + word.substring(0, secondVowelGroup).toLowerCase() + "ay";
                    if (startWithUpperCase) {
                        char firstUpperCaseCharacter = Character.toUpperCase(newWord.charAt(0));
                        newWord = firstUpperCaseCharacter + newWord.substring(1);
                    }
                    newWords.add(newWord);
                } else {
                    newWords.add(word + "yay");
                }
            }
        }

        //Build new string.
        for (int i = 0; i < newWords.size(); i++) {
            output.append(newWords.get(i));
            if (i != newWords.size() - 1) {
                output.append(' ');
            }
        }

        return output.toString();
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
