import math

with open("chars.txt", "r", encoding="utf-8") as f:
    lines = f.readlines()

template = lines[0]
chars = lines[1]
for i, char in enumerate(chars):
    char = ord(char)
    if char > 65536:
        first_surrogate = hex(math.floor((char - 0x10000) / 0x400) + 0xD800)[2:]
        second_surrogate = hex(((char - 0x10000) % 0x400) + 0xDC00)[2:]
        print(f"put(\'{template[i]}\', \"\\u{first_surrogate}\\u{second_surrogate}\");")
    else:
        print(f"put(\'{template[i]}\', \"\\u{hex(char)[2:]}\");")
